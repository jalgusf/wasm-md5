use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn rust_md5(s: &str) -> String {
    format!("{:x}", md5::compute(s))
}

#[cfg(test)]
mod tests {
	use super::*;
    #[test]
    fn it_works() {
        assert_eq!(rust_md5("öäü"), String::from("7448211b4951bf618d8b7688144295ba"));
        assert_eq!(rust_md5("qwcasd qwe243243"), String::from("b0346806250b9f61c13e6bd083b719d4"));
        assert_eq!(rust_md5("asdqweqawe134234"), String::from("b5f29e777f40a9a0179c4cc908a96c84"));
    }
}
