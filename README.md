## Description
A very simple wasm package for calculating md5 hashes from a Javascript environment.

This library basically only wraps md5 with wasm-pack.

## Development
Requires wasm-pack for compiling, e.g. `wasm-pack build --target web`.

The following guides were used and might therefore be useful for further development:
- https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm
- https://rustwasm.github.io/docs/wasm-bindgen/examples/without-a-bundler.html

## Hints for usage
The index.html is simply an example for usage. It exposes the md5 hash function to javascript under the name `md5`.